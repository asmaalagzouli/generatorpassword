const clipboardy = require('clipboardy');


const copyPassword = (password) => {
    clipboardy.writeSync(password);
}


module.exports = copyPassword;